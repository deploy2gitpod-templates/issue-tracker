#!/bin/sh

## Define the custom commands we use in this script.
print_status() {
    echo
    echo "## $1"
    echo 
}

pauseBeforeNextStep() {
    sleep $1
    clear
}

## Define the variables we needed.
GITLAB_TRIAGEOPS_API_TOKEN=$API_TOKEN ## If the API token is defined, please do not steal when remixing.
SOURCE_PROJECT_PATH=deploy2gitpod-templates/issue-tracker ## Change thi when remixing.

## Confirm that GitLab Triage gem is installed successfully.
print_status "Verifying gem installation..."
gitlab-triage --help
pauseBeforeNextStep 5

## Then run the dry-run
print_status "Running dry-runs..."
gitlab-triage --dry-run --token $GITLAB_TRIAGEOPS_API_TOKEN --source-id $SOURCE_PROJECT_PATH
exit 1