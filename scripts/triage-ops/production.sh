#!/bin/sh

## Define the custom commands we use in this script
print_status() {
    echo
    echo "## $1"
    echo 
}

pauseBeforeNextStep() {
    sleep $1
}

## Define the variables we needed.
GITLAB_TRIAGEOPS_API_TOKEN=$API_TOKEN
SOURCE_PROJECT_PATH=deploy2gitpod-templates/issue-tracker

## Confirm that GitLab Triage gem is installed successfully.
print_status "Skipped GitLab Triage installation because we're running production mode."
print_status 5

## Then triage!
print_status "Hang tight, we're now triaging them!"
gitlab-triage --token $GITLAB_TRIAGEOPS_API_TOKEN --source-id $SOURCE_PROJECT_PATH
exit 1