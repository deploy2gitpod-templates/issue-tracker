# Issue Tracker

Use this project to create issues about the templates the Pins team made.

## How to create a new issue?
Our issue tracker is on [GitLab](https://gitlab.com), so if you don't have an account there, please sign up using your GitHub account.
Once you signed in there and you landed here, select the available template for your issue and fill up the templat form.

When ready, hit the *create new issue* button.

## Where I can found the full history of these?
For easy navigation, use the `status-tracker` directory and browse what's there. If there's nothing, come back next time.

## Support our Work
Support our work by donating some money

## Group Maintainers
The `deploy2gitpod-templates` are managed by your friends at @ThePinsTeam, including @AndreiJirohHaliliDev2006.

Want to be an maintainer? [See our handbook for details!](https://en.handbookbythepins.gq/life-at-the-pins/join)
