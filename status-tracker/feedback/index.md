# Feedback Archive

## FAQs
Want to ask a question even still unanswered here about Template Suggestions Archive? While this FAQ is **work in progress*, you can ask questions in the Issue Tracker.

### Why I am seeing `.nodata` in some subdirectories?
If you see the `.nodata` file with the content `There was no data here. Please return here later.`, that's means we don't have enough data yet for now. Come back again next time.

## How do you add them to the archive?
We look for the following labels that matches our requirements. If these meets our requirements, they'll be archived in the next few weeks.

| Requirement Type | Answer |
| ---- | ------ |
| Labels | ~"enchancement" |
| Issue Template Used | `Template feedback` |
| Must be created in | [Group Issue Tracker](https://gitlab.com/deploy2gitpod-templates/issue-tracker/issues) or in the template's issue tracker |
